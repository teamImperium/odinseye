import Vue from 'vue'
import VueRouter from 'vue-router'
import draggable from 'vuedraggable'
import App from './App.vue'
import TimeLine from './components/timeline'
import mensajeria from './components/mensajeria'
import issue from './components/issue'
import assigments from './components/assigments'
import task from './components/task'
import webconfig from './components/webconfig'
import roles from './components/roles'
import login from './components/login'
import upload from './components/upload'
//import socketio from 'socket-client.io'
//import VueSocketio from 'vue-socket.io'

//export const SocketInstance = socketio('http://localhost:1337');

require('./assets/styles/main.scss');


//Vue.use(VueSocketio, SocketInstance);
Vue.use(VueRouter);

const routes = [
	//{path: '/ruta', component: Componente},
  {path: '/', component: login},
	{path: '/inicio', component: mensajeria},
	{path: '/registro', component:TimeLine},
  {path: '/mensajeria', component:mensajeria},
  {path: '/issue', component:issue},
  {path: '/tareas', component: assigments},
  {path:'/task',component:task},
  {path:'/webconfig',component:webconfig},
  {path: '/roles',component:roles},
  {path:'/login',component:login},
  {path:'/upload',component:upload},
];

const router = new VueRouter({
	routes,
	mode: 'history'
});


new Vue({
  render: h => h(App),
  router
}).$mount('#app')

