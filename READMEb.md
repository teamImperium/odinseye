# README #

Odin's Eye, montado en una base de datos relacional y SailsJS. Consiste en un software de gestión de tareas de maquetación/traducción. 

### ¿Para qué es este repositorio? ###

* Cada developer sube su codigo en la branch con su nombre
* Al final del tiempo establecido para hacer cada requerimiento, los miembros del equipo con gremio de Git (Elder) se encargan de hacer el merge y sacar el release. 
* El motivo principal de este repo es mantener nuestro código ordenado. limpio y llevar un control de versiones.


### Guía para el equipo ###

* Una vez el código ha sido testeado y aprobado, se subirá a la branch con tu nombre.
* ¿Con quién hablo para que testee mi código? Con Ricardo.
* Pase lo que pase, NO SUBIR A MASTER. 
* nombre de archivos: minúscula
* variables: underscore
* clases: upper camel case
* funciones: lower camel case
* base de datos: underscore

### BACK-END ###
En este apartado estará la estructura base del backend para el proeycto Odin

### Tengo un problema. ¿Con quién hablo? ###

Ruth o Elder. Cualquier cosa estamos para ayudar. 
