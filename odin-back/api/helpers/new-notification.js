module.exports = {


  friendlyName: 'New notification',


  description: '',


  inputs: {
    taskId: {
      type: 'number',
      example: 1,
      description: 'The id of the task',
      required: true
    },
    threadId: {
      type: 'number',
      example: 1,
      description: 'The id of the thread created',
      required: true      
    },
    description: {
      type: 'string',
      example: 'Hello world',
      description: 'A brief description of the notification for the user to see',
      required: true
    }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },


  fn: async function (inputs) {
    // TODO
    // let usersSubscribed = await Subscription.findAll({
    //   where:{
    //     taskId: inputs.taskId
    //   }
    // });
    // let notifications = [];
    // usersSubscribed.forEach(user => {
    //   let newNotification = await Notifications.create({
    //         'userId' : user.id,
    //         'threadId' : req.body.threadId
    //     })
    //   notifications.push(newNotification);
    //   sails.sockets.broadcast('notifUser'+user.id, 'NewThread', {message: "Nuevo hilo en tarea: "+req.body.taskType});
    // });
  }


};

