/**
 * Subscriptions.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    subscriptionId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: "subscription_id"
    },
    userId : {
      type : Sequelize.INTEGER,
      allowNull : false,
      field: "user_id"
    },
    taskId : {
      type : Sequelize.INTEGER,
      allowNull : false,
      field: "task_id"
    }
  },
  associations: function() {
    Subscriptions.belongsTo(Users, {
      foreignKey: 'userId'
    });
    Subscriptions.belongsTo(Task, {
      foreignKey: 'taskId'
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'subscriptions',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};

