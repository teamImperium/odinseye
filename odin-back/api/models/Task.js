/**
 * Task.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    taskId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: 'task_id'
    },
    taskType: {
      type: Sequelize.STRING,
      allowNull: false,
      field: 'task_type'
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    accepted: {
      type: Sequelize.BOOLEAN,
      allowNull: false
    },
    dateFinish: {
      type: Sequelize.DATE,
      allowNull: false,
      field: 'date_finish'
    },
    timeAccepted: {
      type: Sequelize.DATE,
      allowNull: true,
      field: 'time_accepted'
    },
    taskTypeId: {
      type: Sequelize.INTEGER,
      allowNull: true,
      field: 'task_typeid'
    },
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      field: 'user_id'
    },
    materiaid: {
      type: Sequelize.INTEGER,
      allowNull: true
    }
  },
  associations: function() {
    Task.belongsTo(Users, {
      foreignKey: 'userId'
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'task',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};

