/**
 * Users.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    userId: {
      type : Sequelize.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			field: "user_id"
    },
    username: {
			type: Sequelize.STRING,
			allowNull : false,
		},
		password: {
			type: Sequelize.STRING,
			allowNull : true
		},
		description : {
			type : Sequelize.STRING,
			allowNull : true
		},
    email : {
			type : Sequelize.STRING,
			allowNull : true
    },
    rolId : {
			type : Sequelize.INTEGER,
      allowNull : true,
      field : 'rol_id',
		},
		createdAt : {
			type : Sequelize.DATE,
			allowNull : true,
		},
		updatedAt : {
			type : Sequelize.DATE,
			allowNull : true,
		}
  },
  options: {
    freezeTableName: false,
    tableName: 'users',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};

