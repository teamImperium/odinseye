/**
 * Documents.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    documentId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: "document_id"
    },
    title : {
      type : Sequelize.STRING,
      allowNull : false
    },
    threadId : {
      type : Sequelize.INTEGER,
      allowNull : false
    },
    url: {
      type: Sequelize.STRING,
      allowNull: false
    }
  },
  associations: function() {
    Documents.belongsTo(Threads, {
      foreignKey: 'threadId'
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'documents',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
  }
};
