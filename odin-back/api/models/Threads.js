/**
 * Threads.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    threadId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: "thread_id"
    },
    title : {
      type : Sequelize.STRING,
      allowNull : false
    },
    userId : {
      type : Sequelize.INTEGER,
      allowNull : false,
      field: "user_id"
    },
    taskId : {
      type : Sequelize.INTEGER,
      allowNull : false,
      field: "task_id"
    }
  },
  associations: function() {
    Threads.belongsTo(Users, {
      foreignKey: 'userId'
    });
    Threads.belongsTo(Task, {
      foreignKey: 'taskId'
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'threads',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};

