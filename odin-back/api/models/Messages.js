/**
 * Messages.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    messageId: {
      type : Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: "message_id"
    },
    message : {
			type : Sequelize.STRING,
			allowNull : false,
	
		},
    threadId : {
			type : Sequelize.INTEGER,
      allowNull : true,
      field: 'thread_id'
    },
    userId : {
			type : Sequelize.INTEGER,
      allowNull : true,
      field: 'user_id'
		},
		createdAt : {
			type : Sequelize.DATE,
			allowNull : true,
			field : 'createdAt',
		},
		updatedAt : {
			type : Sequelize.DATE,
			allowNull : true,
			field : 'updatedAt',
		}
  },
  associations: function() {
    Messages.belongsTo(Users, {
      foreignKey: 'userId'
    });
    Messages.belongsTo(Threads, {
      foreignKey: 'threadId'
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'messages',
    classMethods: {},
    instanceMethods: {},
    hooks: {}
  }

};

