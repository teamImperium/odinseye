/**
 * Notifications.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    notificationId: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
      field: "notification_id"
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    userId : {
      type : Sequelize.INTEGER,
      allowNull : false,
      field: "user_id"
    },
    threadId : {
      type : Sequelize.INTEGER,
      allowNull : false,
      field: "thread_id"
    },
  },
  associations: function() {
    Notifications.belongsTo(Users, {
      foreignKey: 'userId'
    });
    Notifications.belongsTo(Threads, {
      foreignKey: 'threadId'
    });
  },
  options: {
    freezeTableName: false,
    tableName: 'notifications',
    classMethods: {},
    instanceMethods: {},
    hooks: {},
  }

};

