/**
 * TaskController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    createTask : async function (req, res){
        let newTask = await Task.create({
            'taskType' : req.query.taskType,
            'description': req.query.description,
            'userId' : req.query.userId,
            'dateFinish': req.query.dateFinish,
            'accepted': false
        });
        //CHANGE TO HELPER LATER BEGINING-----------------------BEGINNING--------------------------
        let newThread = await Threads.create({
            'title' : "Tarea "+ newTask.taskId +" creada",
            'userId' : req.query.userId,
            'taskId' : newTask.taskId,
        });
        let newSubscription = await Subscription.create({
            'userId': req.query.userId,
            'taskId': newTask.taskId
        });
        //CHANGE TO HELPER LATER-----------------------END--------------------------
        
        let taskAndThread = {
            newTask: newTask,
            newThread: newThread
        };
        res.send(taskAndThread);
    },
    getTasks : async function (req, res){
        var taskList = await Task.findAll({
            include: [{
                model: Users, 
                required: true
            }]
        });
        let currentDate = new Date();
        taskList.forEach(item => {
            let daysNum = (Date.parse(item.dataValues.dateFinish)-Date.parse(currentDate))/86400000;
            let daysHoursLeft = daysNum.toString().split(".");
            item.dataValues["daysForDoom"] = daysHoursLeft[0];
        });
        res.send(taskList);
    }

};