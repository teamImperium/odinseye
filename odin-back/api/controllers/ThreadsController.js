/**
 * ThreadsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    onConnect: function(req, res){
         if(!req.isSocket){
             return res.badRequest();
         }
        sails.log("Connecting socket")
        sails.log("thread id :"+req.body.threadId)
        const roomId = req.body.threadId;
        //sails.log.info(req.connection);
        
         sails.sockets.join(req, 'chat'+roomId);
         sails.sockets.broadcast('chat'+roomId, 'UserEnteredChat', {message: 'Usuario conectado al chat'+roomId});
         res.send({success: true})
    },
    createThread : async function (req, res){
        let newThread = await Threads.create({
            'title' : req.query.title,
            'userId' : req.query.userId,
            'taskId' : req.query.taskId,
        });

        //CHANGE TO HELPER LATER BEGINING-----------------------BEGINNING--------------------------
        let usersSubscribed = await Subscriptions.findAll({
            where:{
              taskId: req.query.taskId
            }
        });
        let notifications = [];
        usersSubscribed.forEach(item => {
            notifications.push({
                'userId' : item.dataValues.userId,
                'taskId': req.query.taskId,
                'threadId' : newThread.threadId,
                'description': "Nuevo hilo en tarea: "+req.query.taskId
            });
            sails.sockets.broadcast('notifUser'+item.dataValues.userId, 'NewThread', {message: "Nuevo hilo en tarea: "+req.query.taskId});
        });
        let newNotification = await Notifications.bulkCreate(notifications)
        //CHANGE TO HELPER LATER-----------------------END--------------------------

        res.send(newThread);
    },
    getThreads : async function (req, res){
        //console.log(req.query)
        var threadList = await Threads.findAll({
            where: {
              'taskId': req.query.taskId
            },
            include: [{model: Users, required: true}]
        });
        sails.log(threadList);

        threadList.forEach(singleThread => {
            singleThread.dataValues.createdAt = singleThread.dataValues.createdAt.toISOString().
            replace(/T/, ' ').      // replace T with a space
            replace(/\..+/, '');
        });
        res.send(threadList);

    }

};

