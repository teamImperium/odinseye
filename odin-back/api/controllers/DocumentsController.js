/**
 * DocumentsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    uploadDocument : function(req,res){
        req.file('file').upload({
            dirname: require('path').resolve(sails.config.appPath, 'documents/'+req.body.taskId)
          }, async function (err, uploadedFiles) {
            if (err) return res.serverError(err);
            
            let documents = [];
            sails.log("hello Im here", uploadedFiles);
            uploadedFiles.forEach(item => {
              let path = item.fd.split('documents');
              documents.push({
                title: req.body.fileName,
                threadId: req.body.threadId,
                url: path[1]
              });
            });
            let newDocumentBulk = await Documents.bulkCreate(documents);
            return res.send({
              message: uploadedFiles.length + ' file(s) uploaded successfully!',
              documents: newDocumentBulk
            });
          });
    }
};

