/**
 * MessagesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    createMessage: async function (req, res){
        let newMessage = await Messages.create({
            message: req.query.message,
            threadId: req.query.threadId,
            userId: req.query.userId
        });
        let user = await Users.findOne({
            where:{
                userId: req.query.userId
            }
        });
        sails.log(user);
        newMessage.dataValues["User"] = user.dataValues;
        sails.sockets.broadcast('chat'+newMessage.threadId, "NewMessage", newMessage);
        res.send(newMessage); 
    },
    getMessages: async function (req, res){
        let messageList = await Messages.findAll({
            where: {
                'threadId': req.query.threadId
            },
            include: [
                {
                    model: Users,
                    required: true,
                }
            ]
        });
        messageList.forEach(singleMessage => {
            singleMessage.dataValues.createdAt = singleMessage.dataValues.createdAt.toISOString().
            replace(/T/, ' ').      // replace T with a space
            replace(/\..+/, '');
        });
        res.send(messageList);
    },
};

