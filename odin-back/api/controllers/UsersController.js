/**
 * UsersController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    //TODO: JSON web token authentication Elder
    createUser: async function (req, res){
        let newUser = await Users.create({
            username: req.query.username,
            password: req.query.password,
            email: req.query.email
        });
        res.send(newUser);
    },
    getUser: async function (req, res){
        var userList = await Users.findAll({
            where: {
              'username': req.query.username
            }
        });
        res.send(userList);
    },
    loginCreateUser: async function (req, res){
        var lastUser = await Users.findAll({
            limit: 1,
            order: [['createdAt', 'DESC']]
        });
        newUsernameNumber = lastUser[0].dataValues.userId + 1;
        let newUser = await Users.create({
            username: "Usuario"+newUsernameNumber,
            description: "Usuario prueba",
            email: "user"+newUsernameNumber+"@user.com",
        });
        req.session.iduser = newUser.userId;
        res.send(newUser);
    },
    login: async function (req, res){
        let userName = req.query.username;
        let userPass = req.query.password;
        let loggedUser = await Users.findOne({
            where:{
                username: userName,
                password: userPass
            }
        });
        sails.log(loggedUser);
        req.session.iduser = loggedUser.dataValues.userId;
        req.session.username = loggedUser.dataValues.username;
        res.send(loggedUser);
    }
};

