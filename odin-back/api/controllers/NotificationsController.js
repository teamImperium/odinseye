/**
 * NotificationsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    triggerNotification : function(req,res){
        sails.log(req.query.channelid)

        const channelToSend = req.query.channelid;
        sails.sockets.broadcast('notifUser'+channelToSend, 'UserSubscribedToNotification', {message: "Nueva notificación"});
        res.send({success: true})
    },
    newThreadOnTask : function(req, res){
        //CUANDO HAY UN NUEVO CAMBIO EN EL DOCUMENTO (NUEVO THREAD) SE NOTIFICA A LOS QUE ESTÁn SUSCRITOS
        const usersSubscribed = req.body.users;
        usersSubscribed.forEach(user => {
            Notifications.create({
                'usersId' : user.id,
                'threadId' : req.body.threadId
            });
            sails.sockets.broadcast('notifUser'+user.id, 'NewThread', {message: "Nuevo hilo en tarea: "+req.body.taskType});
        });
    },
    getUserNotifications: async function(req, res){
        //CHANGE TO HELPER LATER BEGINING-----------------------BEGINNING--------------------------
        let userTasks = await Subscriptions.findAll({
            where:{
              userId: req.query.userId
            },
            include: [
                {
                    model: Task,
                    required: true,
                }
            ]
        });
        let notifications = await Notifications.findAll({
            where: {
                userId: req.query.userId
            },
            include: [{
                model: Threads, 
                required: true
            }]
        });
        let currentDate = new Date();
        userTasks.forEach(item => {
            // let daysNum1 = (new Date(item.dataValues.Tasks[0].dataValues.dateFinish).getTime()-currentDate.getTime())/86400000;
            // let number1 = daysNum1.toString().split(".");
            let daysNum = (Date.parse(item.dataValues.Task.dataValues.dateFinish)-Date.parse(currentDate))/86400000;
            let daysHoursLeft = daysNum.toString().split(".");
            let daysLeft = parseInt(daysHoursLeft[0]);
            let description = "";
            if(daysLeft<=5 && daysLeft>1)
                description = "Te quedan "+daysLeft+" dias para terminar la tarea " + item.dataValues.Task.dataValues.taskId;
            else if(daysLeft==1)
                description = "Te queda "+daysLeft+" dia para terminar la tarea " + item.dataValues.Task.dataValues.taskId;
            else if(daysLeft==0)
                description = "Hoy es el dia de entrega!";
            
            notifications.push({
                'notificationId': 0,
                'userId': req.query.userId,
                'taskId': item.dataValues.Task.dataValues.taskId,
                'threadId' : 0,
                'description': description
            });
        });
        //CHANGE TO HELPER LATER BEGINING-----------------------END--------------------------
        res.send(notifications);
    }

};

