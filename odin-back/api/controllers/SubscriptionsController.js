/**
 * SubscriptionsController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    subscribeUserToChannel: function(req, res){
        if(!req.isSocket){
            return res.badRequest();
        }

       sails.log("Trying to subscribe to notifications user id :", req.body.userId)
       const channelId = req.body.userId
       
       //sails.log.info(req.connection);
        sails.log("subscribing "+req.body.userId)
        sails.sockets.join(req, 'notifUser'+channelId);
        sails.sockets.broadcast('notifUser'+channelId, 'UserSubscribedToNotification', {message: 'Usuario subscrito al canal'+channelId});
        res.send({success: true})
    },

    subscribeTaskToUsers : async function(req, res){
        //AL MOMENTO DE CREAR UNA TAREA, SUBSCRIBIR A LOS USUARIOS A DICHA TAREA
        // subscriptionsList.push({
        //     'taskId' : req.query.taskId,
        //     'userId' : req.query.userId,
        // });
        let newSubscription = await Subscriptions.create({
            'taskId' : req.query.taskId,
            'userId' : req.query.userId,
        });
        req.send(newSubscription);
    }

};

