/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * https://sailsjs.com/docs/concepts/logging
 */
// const { createLogger, format, transports }  = require('winston');

// const myFormat = format.printf(info => {
//   return `${info.timestamp} ${info.level}: ${info.message}`;
// });

// const logger = createLogger({
//   level: "silly",
//   json: false,
//   format: format.combine(  
//     format.timestamp(),
//     myFormat
//   ),
//   timestamp: true, // if you want to output the timestamp in the console transport
//   prettyPrint: true,
//   transports: [
//     //
//     // - Write to all logs with level `info` and below to `combined.log` 
//     // - Write all logs error (and below) to `error.log`.
//     //
//     new transports.Console({ level: 'silly', format: format.combine(    
//       format.colorize(),
//       format.timestamp(),
//       myFormat
//     )}),
//     new transports.File({ filename: 'error.log', level: 'error', handleExceptions: true}),
//     new transports.File({ filename: 'info.log', level: 'info'}),
//     new transports.File({ filename: 'combined.log', handleExceptions: true})
//   ]
// });

// logger.exceptions.handle(
//   new transports.File({ filename: 'exceptions.log' })
// );

// logger.on('finish', function (info) {
//   // All `info` log messages has now been logged
//   logger.info('------------------- FINESHED LOGS! --------------------', { seriously: true });
// });
module.exports.log = {

  /***************************************************************************
  *                                                                          *
  * Valid `level` configs: i.e. the minimum log level to capture with        *
  * sails.log.*()                                                            *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, verbose, info, debug, warn, error                                 *
  *                                                                          *
  * You may also set the level to "silent" to suppress all logs.             *
  *                                                                          *
  ***************************************************************************/
//  custom: logger,
 // Disable captain's log so it doesn't prefix or stringify our meta data.
//  inspect: false

};
