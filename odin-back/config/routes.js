/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
  
  'GET /tasks/createTask' : 'TaskController.createTask',
  'GET /tasks/getTasks' : 'TaskController.getTasks',
  'GET /threads/createThread' : 'ThreadsController.createThread',
  'GET /threads/getThreads' : 'ThreadsController.getThreads',
  "GET /users/createUser": 'UsersController.createUser',
  "GET /users/getUser": 'UsersController.getUser',
  "GET /users/login": 'UsersController.login',
  "GET /users/getUserTasks": "UsersController.getTasksStatus",
  "GET /messages/createMessage": 'MessagesController.createMessage',
  "GET /messages/getMessages": 'MessagesController.getMessages',
  "POST /threads/subscribeToThread" : "ThreadsController.OnConnect",
  "POST /subscriptions/subscribeUserToChannel" : "SubscriptionsController.subscribeUserToChannel",
  "GET /notifications/sendNotification" : "NotificationsController.triggerNotification",
  "GET /notifications/getUserNotifications": 'NotificationsController.getUserNotifications',
  "POST /documents/uploadDocument":"DocumentsController.uploadDocument",
};
